import unittest
import os
import synchronizer


class TestSynchronizer(unittest.TestCase):

  #=========================================================================
  # test_a
  #  drive ids are part of a set and both drives have the appropriate
  #  id files
  # test_b
  #  drive ids are part of different sets
  # test_c
  #  only one drive has an id file
  # test_d
  #  both drives have id files that are not part of any set
  # test_e
  #  both drives do not have id files
  # test_f
  #  both drives have id files similar to those in the drive sets
  #  ex.  6a49b879_633 and 6a49b879_4466
  #=========================================================================

  def test_get_drive_type(self):
    self.assertEqual('', synchronizer.get_drive_type('abc'))
    self.assertEqual('source', synchronizer.get_drive_type('6a49b879_63'))
    self.assertEqual('backup', synchronizer.get_drive_type('6a49b879_446'))
    
  def test_get_drive_id(self):
    file_list_1_empty   = []
    file_list_2_no_id   = ['a', 'b', 'c']
    file_list_3_with_id = ['a', 'b', '6a49b879_63.txt']
    
    self.assertEqual('', synchronizer.get_drive_id(file_list_1_empty))
    self.assertEqual('', synchronizer.get_drive_id(file_list_2_no_id))
    self.assertEqual('6a49b879_63', synchronizer.get_drive_id(file_list_3_with_id))

  def test_get_drive_set(self):
    self.assertEqual('set1', synchronizer.get_drive_set('6a49b879_63'))
    
  def test_start_sync(self):
    drive_a = []
    drive_b = ['a.txt','b.txt','c.txt']
    drive_c = ['a.txt','6a49b879_63.txt']          # source drive - set1
    drive_d = ['6a49b879_466.txt','a.txt','b.mp3'] # backup drive - set2
    drive_e = ['6a49b879_74.txt','a.txt','b.mp3']  # source drive - set2
    drive_f = ['6a49b879_95.txt','a.txt','b.mp3']  # backup drive - set3
    drive_g = ['6a49b879_459.txt','a.txt','b.mp3'] # backup drive - set3
    
    drives_is_a_set         = [drive_d, drive_e]
    drives_all_empty_drives = [drive_a, drive_a]
    drives_one_empty_drive  = [drive_a, drive_c]
    drives_not_a_set        = [drive_c, drive_d]
    drives_both_source      = [drive_c, drive_e]
    drives_same             = [drive_c, drive_c]
    drives_one_no_id        = [drive_b, drive_c]
    drives_backups_same_set = [drive_f, drive_g]
    
    self.assertTrue(synchronizer.start_sync(drives_is_a_set[0],
                                            drives_is_a_set[1]))
    self.assertFalse(synchronizer.start_sync(drives_all_empty_drives[0],
                                             drives_all_empty_drives[1]))
    self.assertFalse(synchronizer.start_sync(drives_one_empty_drive[0],
                                             drives_one_empty_drive[1]))
    self.assertFalse(synchronizer.start_sync(drives_not_a_set[0],
                                             drives_not_a_set[1]))
    self.assertFalse(synchronizer.start_sync(drives_both_source[0],
                                             drives_both_source[1]))
    self.assertFalse(synchronizer.start_sync(drives_same[0],
                                             drives_same[1]))
    self.assertFalse(synchronizer.start_sync(drives_one_no_id[0],
                                             drives_one_no_id[1]))
    self.assertFalse(synchronizer.start_sync(drives_backups_same_set[0],
                                             drives_backups_same_set[1]))

if __name__ == '__main__':
   unittest.main()
