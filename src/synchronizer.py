import os
import subprocess
import platform

'''
This tool synchronizes only two disks at a time.
'''
ID_STRING = '6a49b879'

def get_drive_sets():
  '''
  This is where to make changes to the drive inventory.
  '''
  # TODO add set9
  sets = {
      'set1':{'source':'6a49b879_63', 'backup':['6a49b879_446'                ]},
      'set2':{'source':'6a49b879_74', 'backup':['6a49b879_466'                ]},
      'set3':{'source':'6a49b879_68', 'backup':['6a49b879_95' , '6a49b879_459']},
      'set4':{'source':'6a49b879_48', 'backup':['6a49b879_477'                ]},
      'set5':{'source':'6a49b879_69', 'backup':['6a49b879_482'                ]},
      'set6':{'source':'6a49b879_62', 'backup':['6a49b879_476'                ]},
      'set7':{'source':'6a49b879_70', 'backup':['6a49b879_462'                ]},
      'set8':{'source':'6a49b879_44', 'backup':['6a49b879_481'                ]},
      'set9':{'source':''           , 'backup':[''                            ]}
      }

  return sets

def generate_drives_lists():
  '''
  Return dictionary containing a list of source and backup drives.

  {
    'all'   :[a, b, c, x, y, z],
    'source':[a, b, c],
    'backup':[x, y, z]}
  '''
  sets   = get_drive_sets()
  result = {
      'all'   :[],
      'source':[],
      'backup':[]}

  for drive_set in sets:
    result['all'   ].append(sets[drive_set]['source'])
    result['all'   ] +=     sets[drive_set]['backup']
    result['source'].append(sets[drive_set]['source'])
    result['backup'] +=     sets[drive_set]['backup']

  return result

def get_drive_set(id):
  # TODO test
  result = ''
  sets = get_drive_sets()
  for set in sets:
    if sets[set]['source'] == id or id in sets[set]['backup']:
      result = set

  return result

def get_drive_type(id):
  '''
  Return 'source' or 'backup' depending on whether given id is in source or
  backup drives list.
  '''
  drives     = generate_drives_lists()
  drive_type = ''

  if id in drives['source']:
    drive_type = 'source'
  elif id in drives['backup']:
    drive_type = 'backup'

  return drive_type

def get_drive_id(drive_path):
  '''
  Return file name of id file of a drive represented by the given drive path
  (ie. return file name without the file extension).
  id file, if exists, is found at root level of given drive path.
  '''
  drive_id = ''
  files    = ''
  try:
    if os.path.isdir(drive_path):
      files = os.listdir(drive_path)
  except TypeError:
    files = drive_path

  for file_name in files:
    if ID_STRING in file_name:
      drive_id = file_name[:file_name.find('.txt')]
  return drive_id

def get_drive_with_type(type, drive_a_info, drive_b_info):
  result = ''

  if drive_a_info['type'] == type:
    result = drive_a_info['path']
  else:
    result = drive_b_info['path']

  return result

def get_drive_info(drive_path):
  drive_info         = {'type':'', 'id':'', 'set':'', 'path':drive_path}
  drive_info['id']   = get_drive_id(drive_path)
  drive_info['type'] = get_drive_type(drive_info['id'])
  drive_info['set']  = get_drive_set(drive_info['id'])

  return drive_info

def start_sync(drive_a_path, drive_b_path, is_test=None):
  '''
  Start rsync process. Should only be allowed to happen when drives
  are set and if executing on linux, else return False.
  Return True if conditions that have been satisfied that allow rsync to be
  called.
  '''

  # for safety, need to explicitly set this function parameter 
  # to False in order for rsync to be called
  is_test      = is_test or True

  drive_a_info = get_drive_info(drive_a_path)
  drive_b_info = get_drive_info(drive_b_path)
  source_drive = get_drive_with_type('source', drive_a_info, drive_b_info)
  backup_drive = get_drive_with_type('backup', drive_a_info, drive_b_info)

  are_drives_in_same_set = drive_a_info['set'] == drive_b_info['set']
  have_source_and_backup = drive_a_info['type'] != drive_b_info['type']
  is_run_on_linux        = platform.system().lower() == 'linux'

  print drive_a_info
  print drive_b_info
  # this segment serves for testing purposes. setting up folders and accessing 
  # them through python is tedious. the drive paths are eventually converted to
  # file lists anyway so lists can be passed as arguments to the function to 
  # simplify testing. the actual drive paths are only needed by the 'else' 
  # portion that initialize the parameters for rsync.

  if (are_drives_in_same_set and have_source_and_backup and is_run_on_linux):
    if is_test:
      print 'rsync would have run.'
      print 'source drive:', source_drive
      print 'backup drive:', backup_drive
    else:
      subprocess.call(['sudo', 'rsync', '-ahvt', '--progress',
        '--delete', '--stats', '--exclude', '*.txt',
        (source_drive + '/'), backup_drive])
    return True
  else:
    return False
