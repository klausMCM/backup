'''
Created on Nov 6, 2014

@author: Klaus
'''
import integrity
import json
import os

if __name__ == '__main__':
    print 'enter root directory to be processed'
    working_dir = raw_input('>> ')
    print 'enter filetypes to be added to database (comma separated)'
    file_types = raw_input('>> ')
    hash_type = 'sha256'
    reference_file_path = os.path.join(working_dir, 'reference.json')
    
    reference_db = json.loads(
        integrity.generate_json_db(working_dir, hash_type, True, True, file_types))
    current_db = json.loads(
        integrity.generate_json_db(working_dir, hash_type, False, False, file_types))
    integrity.update_json_db_file(reference_file_path,
                                 integrity.add_new_files_to_db(reference_db,
                                                              current_db))
    integrity.update_json_db_file(reference_file_path,
                                 integrity.remove_deleted_files_from_db(reference_db,
                                                                       current_db))
    integrity.verify_hash_values(reference_db, current_db, working_dir)
    print 'DONE'
