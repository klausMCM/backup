'''
Created on Nov 4, 2014

@author: Klaus
'''
import unittest
from integrity import is_new
from integrity import is_deleted
from integrity import add_new_files_to_db
from integrity import remove_deleted_files_from_db
from integrity import find_file_entry
from integrity import verify_hash_values
from integrity import is_file_to_be_added


class Test(unittest.TestCase):

    def setUp(self):
        self.entry1 = {"comments": "", 
                       "date": "20141104", 
                       "file_info": {
                                     "drive_letter": "C:",
                                     "file_name_original": "a", 
                                     "file_path_full": "\\top",
                                     "file_path_relative": "",
                                     "file_suffix": "x"
                                     }, 
                       "hash_info": {
                                     "hash_type": "sha256",
                                     "hash_value": "123"}
                       }
        self.entry2 = {"comments": "", 
                       "date": "20141104", 
                       "file_info": {
                                     "drive_letter": "C:",
                                     "file_name_original": "b", 
                                     "file_path_full": "\\top\\middle",
                                     "file_path_relative": "\\middle",
                                     "file_suffix": "y"
                                     }, 
                       "hash_info": {
                                     "hash_type": "sha256",
                                     "hash_value": "abc"
                                     }
                       }
        self.entry3 = {"comments": "", 
                       "date": "20141104", 
                       "file_info": {
                                     "drive_letter": "C:",
                                     "file_name_original": "c", 
                                     "file_path_full": "\\top\\middle\\bottom",
                                     "file_path_relative": "\\middle\\bottom",
                                     "file_suffix": "z"
                                     }, 
                       "hash_info": {
                                     "hash_type": "sha256",
                                     "hash_value": "456"}
                       }

    def tearDown(self):
        pass

    def test_is_new(self):
        reference_py_object = []
        self.assertTrue(is_new(self.entry1, reference_py_object))

        reference_py_object = [self.entry1]
        self.assertFalse(is_new(self.entry1, reference_py_object))

        reference_py_object = [self.entry2]
        self.assertTrue(is_new(self.entry1, reference_py_object))
        self.assertFalse(is_new(self.entry2, reference_py_object))

        reference_py_object = [self.entry1, self.entry2]
        self.assertFalse(is_new(self.entry1, reference_py_object))
        self.assertFalse(is_new(self.entry2, reference_py_object))

        reference_py_object = [self.entry1, self.entry2, self.entry3]
        self.assertFalse(is_new(self.entry1, reference_py_object))
        self.assertFalse(is_new(self.entry2, reference_py_object))
        self.assertFalse(is_new(self.entry3, reference_py_object))

    def test_is_deleted(self):
        current_py_object = []
        self.assertTrue(is_deleted(self.entry1, current_py_object))

        current_py_object = [self.entry1]
        self.assertFalse(is_deleted(self.entry1, current_py_object))
        self.assertTrue(is_deleted(self.entry2, current_py_object))

        current_py_object = [self.entry1, self.entry2]
        self.assertFalse(is_deleted(self.entry1, current_py_object))
        self.assertFalse(is_deleted(self.entry2, current_py_object))
        self.assertTrue(is_deleted(self.entry3, current_py_object))

    def test_add_new_files_to_db(self):
        reference_py_object = []
        current_py_object = []
        self.assertEqual(len(add_new_files_to_db(reference_py_object,
                                                 current_py_object)), 0)

        reference_py_object = []
        current_py_object = [self.entry1]
        self.assertEqual(len(add_new_files_to_db(reference_py_object,
                                                 current_py_object)), 1)

        reference_py_object = [self.entry1]
        current_py_object = [self.entry1]
        self.assertEqual(len(add_new_files_to_db(reference_py_object,
                                                 current_py_object)), 1)

        reference_py_object = [self.entry1]
        current_py_object = [self.entry1, self.entry2]
        self.assertEqual(len(add_new_files_to_db(reference_py_object,
                                                 current_py_object)), 2)

        reference_py_object = []
        current_py_object = [self.entry1, self.entry2]
        self.assertEqual(len(add_new_files_to_db(reference_py_object,
                                                 current_py_object)), 2)

        reference_py_object = [self.entry1]
        current_py_object = []
        self.assertEqual(len(add_new_files_to_db(reference_py_object,
                                                 current_py_object)), 1)

    def test_remove_deleted_files_from_db(self):
        reference_py_object = []
        current_py_object = []
        self.assertEqual(len(remove_deleted_files_from_db(reference_py_object,
                                                          current_py_object)), 0)

        reference_py_object = [self.entry1]
        current_py_object = []
        self.assertEqual(len(remove_deleted_files_from_db(reference_py_object,
                                                          current_py_object)), 0)

        reference_py_object = [self.entry1, self.entry2]
        current_py_object = []
        self.assertEqual(len(remove_deleted_files_from_db(reference_py_object,
                                                          current_py_object)), 0)

        reference_py_object = [self.entry1, self.entry2]
        current_py_object = [self.entry1]
        self.assertEqual(len(remove_deleted_files_from_db(reference_py_object,
                                                          current_py_object)), 1)

        reference_py_object = [self.entry1, self.entry2]
        current_py_object = [self.entry1, self.entry2]
        self.assertEqual(len(remove_deleted_files_from_db(reference_py_object,
                                                          current_py_object)), 2)

    def test_find_file_entry(self):
        reference_py_object = []
        self.assertEqual(
            None, find_file_entry(self.entry1, reference_py_object))

        reference_py_object = [self.entry1]
        self.assertEqual(
            self.entry1, find_file_entry(self.entry1, reference_py_object))

        reference_py_object = [self.entry1, self.entry2, self.entry3]
        self.assertEqual(
            self.entry2, find_file_entry(self.entry2, reference_py_object))

    def test_verify_hash_values(self):
        entry1_0 = {"comments": "", 
                    "date": "20141104", 
                    "file_info": {
                                  "drive_letter": "C:",
                                  "file_name_original": "a", 
                                  "file_path_full": "\\top",
                                  "file_path_relative": "",
                                  "file_suffix": "x"
                                  }, 
                    "hash_info": {
                                  "hash_type": "sha256",
                                  "hash_value": "123"}
                    }
        entry1_1 = {"comments": "", 
                    "date": "20141104", 
                    "file_info": {
                                  "drive_letter": "C:",
                                  "file_name_original": "a", 
                                  "file_path_full": "\\top",
                                  "file_path_relative": "",
                                  "file_suffix": "x"
                                  }, 
                    "hash_info": {
                                  "hash_type": "sha256",
                                  "hash_value": "124"}
                    }
        entry2 = {"comments": "", 
                    "date": "20141104", 
                    "file_info": {
                                  "drive_letter": "C:",
                                  "file_name_original": "b", 
                                  "file_path_full": "\\top\\middle",
                                  "file_path_relative": "\\middle",
                                  "file_suffix": "y"
                                  }, 
                    "hash_info": {
                                  "hash_type": "sha256",
                                  "hash_value": "abc"}
                    }

        reference_py_object = [entry1_0]
        current_py_object = [entry1_0]
        self.assertEqual(0, len(verify_hash_values(reference_py_object,
                                                   current_py_object, None)))

        reference_py_object = [entry1_0]
        current_py_object = [entry1_1]
        self.assertEqual(1, len(verify_hash_values(reference_py_object,
                                                   current_py_object, None)))

        reference_py_object = [entry2]
        current_py_object = [entry1_0]
        self.assertEqual(0, len(verify_hash_values(reference_py_object,
                                                   current_py_object, None)))

    def test_is_file_to_be_added(self):
        self.assertFalse(is_file_to_be_added("reference.json", None))
        self.assertFalse(is_file_to_be_added("reference.json", None))
        self.assertFalse(is_file_to_be_added("reference.json", "a, b, c"))
        self.assertFalse(is_file_to_be_added("reference.json", "json"))
        self.assertFalse(is_file_to_be_added("results.json", "json"))
        self.assertFalse(is_file_to_be_added("results.json", None))
        self.assertTrue(is_file_to_be_added("a.mp3", None))
        self.assertTrue(is_file_to_be_added("a.mp3", ""))
        self.assertTrue(is_file_to_be_added("a.mp3", "jpg, avi, mp3"))
        self.assertFalse(is_file_to_be_added("a.mp3", "jpg, avi"))
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
