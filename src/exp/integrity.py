'''
Created on Oct 13, 2014

@author: Klaus
'''
#=========================================================================
# need function to:
# 1. find files that need to be copied to the backup directory
# 2. find files that need to be deleted from the backup directory
# 3. check integrity of files in source directory
#  3.1 (COMPLETE) if file already exists in database then compare hash value
#  3.2 (COMPLETE) if file doesn't exist in database, then generate a
#    hash value and add to database
#  3.3 (COMPLETE) if file exists in database but no longer exists in the
#    directory, remove the database entry
#=========================================================================

#=========================================================================
# http://jsonlint.com/
#=========================================================================
import platform
import tempfile
import datetime
import hashlib
import json
import os

# TODO: address above points 1 and 2
# TODO: add ability to obtain hash value of audio data in audio files
#    (mp3, flac, etc.)


def generate_checksum(path_to_file, block_size=2 ** 20, hash_type=None):
  '''
  Return hash object of given file.

  :param path_to_file:
    Includes file name.
  :param block_size:
  :param hash_type:
  '''
  # TODO: need to generate specific hash for *.mp3 and *.flac files
  os_type  = platform.system().lower()
  temp_dir = tempfile.gettempdir()
  
  if hash_type == 'sha256':
    checksum = hashlib.sha256()
  elif hash_type == 'sha1':
    checksum = hashlib.sha1()
  else:
    hash_type = 'md5'
    checksum = hashlib.md5()
    
  #if file type is a mp3 file:
  #  then decode mp3 to wav
  #  then generate hash value of wav
  #else if file type is a flac file:
  #  then decode flac to wav
  #  then generate hash value of wav
  # should also take into consideration whether running on 'windows or 'linux'
  if path_to_file[:-5] == '.flac':
    pass # decode flac to wav and find hash of wav file
  else if path_to_file[:-4] == '.mp3':
    pass # decode mp3 to wav and find hash of wav file
    
  try:
    f = open(path_to_file, 'rb')
    while True:
      data = f.read(block_size)
      if not data:
        break
      checksum.update(data)
  except IOError:
    pass
  finally:
    f.close()
  return checksum


def generate_json_db(source_dir, hash_type, is_reference=None, write_to_file=None,
           file_types=None):
  '''
  Return a json object containing information about all the files in the given
  source directory. Can also write the json data to a file.

  :param source_dir:
  :param is_reference:
    True if generating the reference file which is used as a reference
    file to compare future files to.
  :param write_to_file:
  '''
  time_info                   = datetime.datetime.today()
  is_reference                = is_reference or False
  write_to_file               = write_to_file or False
  has_existing_reference_file = False
  json_db                     = []

  if is_reference:
    for f in os.listdir(source_dir):
      if f == 'reference.json':
        has_existing_reference_file = True
        json_db = json.load(open(os.path.join(source_dir, 'reference.json')))
        break

  if not has_existing_reference_file:
    for root, dirs, files in os.walk(source_dir):
      for f in files:
        if not is_file_to_be_added(f, file_types):
          continue
        #==========================================================
        # ex. C:\Users\Klaus\Desktop\doc.txt
        #      file_name   :  doc
        #      file_suffix :  txt
        #      file_path   :  \Users\Klaus\Desktop
        #      drive_letter:  C:
        #      hash_sha256 :  abc123...
        #      date        :  20140101
        #==========================================================
        json_db.append({'file_info': {
          'file_name_original': os.path.splitext(f)[0],
          'file_suffix'       : (os.path.splitext(f)[1][1:]).lower(),
          'file_path_full'    : os.path.splitdrive(root)[1],
          'file_path_relative': root[len(source_dir):],
          'drive_letter'      : os.path.splitdrive(root)[0],
          'date_last_modified': int(datetime.datetime.fromtimestamp(
            os.path.getmtime(os.path.join(root, f))).strftime('%Y%m%d'))
        },
          'hash_info' : {
          'hash_type' : hash_type,
          'hash_value': generate_checksum(os.path.join(root, f),
                          hash_type=hash_type).hexdigest()
        },
          'date'    : int((str(time_info.year) +
                 str('%02d' % time_info.month) +
                 str('%02d' % time_info.day))),
          'comments': ''})
    if write_to_file:
      if is_reference:
        output_file = open(os.path.join(source_dir, 'reference.json'), 'w')
      else:
        output_file = open(os.path.join(source_dir, 'new.json'), 'w')
      json.dump(json_db, output_file, ensure_ascii=False, sort_keys=True)
  json_db = json.dumps(json_db, ensure_ascii=False)
  return json_db


def is_file_to_be_added(file_name, file_types):
  '''
  Return boolean depending on whether the given file's type matches the given
  file types allowed.

  :param file_name:
  :param file_types:
    String listing file types to be added to database (lowercase and comma-
    separated).
  '''
  file_type = (os.path.splitext(file_name)[1][1:]).lower()
  if file_types == None or file_types == '':
    file_types = []
  else:
    file_types = file_types.replace(' ', '')
    file_types = file_types.split(',')
  result = False

  if file_name == 'reference.json' or file_name == 'results.json':
    result = False
  elif file_type in file_types or len(file_types) == 0:
    result = True
  else:
    result = False

  return result


def verify_hash_values(reference_db_py_object, current_db_py_object, working_dir=None):
  '''
  Return list of entries that currently do not match reference hash values.
  (tested)

  :param reference_db_py_object:
  :param current_db_py_object:
  '''
  discrepancies = []
  for entry_current in current_db_py_object:
    if is_new(entry_current, reference_db_py_object):
      pass
    else:
      entry_reference = find_file_entry(
        entry_current, reference_db_py_object)
      if (entry_reference['hash_info']['hash_value'] !=
          entry_current['hash_info']['hash_value']):
        discrepancies.append(entry_current)
  if working_dir != None:
    output = os.path.join(working_dir, 'results.json')
    json.dump(discrepancies, open(output, 'w'), sort_keys=True)
  return discrepancies


def find_file_entry(entry, db_py_object):
  '''
  Return entry with parameters matching those of the given entry.
  (tested)

  :param entry:
  :param db_py_object:
  '''
  result = None
  for e in db_py_object:
    if ((e['file_info']['file_name_original'] ==
       entry['file_info']['file_name_original']) and
        (e['file_info']['file_suffix'] ==
         entry['file_info']['file_suffix']) and
        (e['file_info']['file_path_full'] ==
         entry['file_info']['file_path_full'])):
      result = e
      break
  return result


def update_json_db_file(file_path, db_py_object):
  f = open(file_path, 'w')
  json.dump(db_py_object, f, ensure_ascii=False, sort_keys=True)
  f.close()


def add_new_files_to_db(reference_db_py_object, current_db_py_object):
  '''
  Return python object.
  (tested)

  :param reference_db_py_object:
  :param current_db_py_object:
  '''
  for entry in current_db_py_object:
    if is_new(entry, reference_db_py_object):
      reference_db_py_object.append(entry)
  return reference_db_py_object


def is_new(entry_from_current_json, reference_db_py_object):
  '''
  Determine whether the file specified in the given entry exists in the json
  database. Return True if exists, else return False.
  The intention of this function is to take every entry in the newly
  generated database and look for its entry in the reference database.
  Comparison is based on
  a) file name
  b) file suffix (*.txt, *.exe, *.mp3, etc.)
  c) file path
  (tested)

  :param reference_db_py_object:
  :param entry_from_current_json:
  '''
  result = True
  for entry in reference_db_py_object:
    if ((entry['file_info']['file_name_original'] ==
          entry_from_current_json['file_info']['file_name_original']) and
        (entry['file_info']['file_suffix'] ==
          entry_from_current_json['file_info']['file_suffix']) and
        (entry['file_info']['file_path_full'] ==
          entry_from_current_json['file_info']['file_path_full'])):
      result = False
      break
  return result


def remove_deleted_files_from_db(reference_db_py_object, current_db_py_object):
  '''
  Return python object.
  (test)

  :param reference_db_py_object:
  :param current_db_py_object:
  '''
  result = []
  for entry in reference_db_py_object:
    if not is_deleted(entry, current_db_py_object):
      result.append(entry)
  return result


def is_deleted(entry, current_db_py_object):
  '''
  Determine whether given entry still exists as a file.
  The intention of this function is to take every entry in the reference
  database and look for an entry in the current database.
  Return True if given entry is no longer in the newly generated database, 
  else return False.
  Comparison is based on
  a) file name
  b) file suffix (txt, exe, mp3, etc.)
  c) file path
  (tested)

  :param entry:
  :param current_db_py_object:
  '''
  result = True
  for e in current_db_py_object:
    if (e['file_info']['file_name_original'] == 
          entry['file_info']['file_name_original'] and
        e['file_info']['file_suffix'] == 
          entry['file_info']['file_suffix'] and
        e['file_info']['file_path_full'] == 
          entry['file_info']['file_path_full']):
      result = False
  return result


class FileCoDel:
  files_to_copy   = []  # copying usually occurs from source to backup
  files_to_delete = []  # deleting usually occurs in backup

  def __init__(self):
    pass
