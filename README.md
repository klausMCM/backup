#Hard drive synchronizer

This tool automates synchronising hard drives, two at a time.

The two drives must be part of a set. Each set consists of one source and at least one backup drive.
Each drive contains at least one text file (*.txt). Only their file name is of interest and represents the drive's unique id (arbitrary). In a database, maintained seperately, the drive's id is mapped to its role (source or backup drive).
```
ex.
	drive A (source): 6a49b879_44.txt
	drive B (backup): 6a49b879_481.txt
```	
Assuming that those two drives belong in the same set, the script will go ahead and sync the two drives (using rsync). If the drives are from different sets, the program will throw an exception.